Name:           attachup
Version:        0.5.2
Release:        6%{?dist}
Summary:        Backup a thumbdrive, when attached, to your computer.

License:        GPLv3
URL:            https://gitlab.com/slackermedia/%{name}
Source0:        https://gitlab.com/slackermedia/%{name}/-/archive/%{version}/%{name}-%{version}.tar.bz2
Vendor:         Slackermedia

BuildArch:      noarch

Requires:       python36 librsync rdiff-backup python3-pyudev

%description
Sync your files from your thumbdrive to your hard drive whenever you attach your thumbdrive to your computer. With this method, you always have the latest version of your thumbdrive backed up to your personal computer, so if your thumbdrive gets destroyed you have lost no data. Or if your computer dies, you still have your thumbdrive.

%prep
%setup -n %{name}-%{version}

%build

%install
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/udev/rules.d
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/{man5,man8}
mkdir -p $RPM_BUILD_ROOT/%{_docdir}

#patch for RHEL-base
sed -i 's_/sbin/mount_/bin/mount_' src/%{name}.py 
sed -i 's_plugdev_users_g' src/%{name}.py 
sed -i 's_/sbin/umount_/bin/umount_' src/80-local.rules
sed -i 's_plugdev_users_g' src/80-local.rules

install -m 755 src/%{name}.py $RPM_BUILD_ROOT/%{_bindir}/%{name}
install -m 644 src/80-local.rules $RPM_BUILD_ROOT/%{_sysconfdir}/udev/rules.d/81-%{name}.rules
install -m 644 src/attachup.conf $RPM_BUILD_ROOT/%{_sysconfdir}
install -m 644 src/example-multi_user-backup.sh $RPM_BUILD_ROOT/%{_docdir}
install -m 644 src/example-single_user-backup.sh $RPM_BUILD_ROOT/%{_docdir}
gzip -r -c src/attachup.8 > $RPM_BUILD_ROOT/%{_mandir}/man8/%{name}.8.gz
gzip -r -c src/attachup.conf.5 > $RPM_BUILD_ROOT/%{_mandir}/man5/%{name}.conf.5.gz

# cleanup
# rm -rf $RPM_BUILD_ROOT

%files
%{_bindir}/%{name}
%{_sysconfdir}/udev/rules.d/81-%{name}.rules
%{_sysconfdir}/%{name}.conf

%doc
%{_docdir}/example-multi_user-backup.sh
%{_docdir}/example-single_user-backup.sh
%{_mandir}/man5/%{name}.conf.5.gz
%{_mandir}/man8/%{name}.8.gz

%changelog
* Thu May 16 2019 Klaatu <klaatu@fedoraproject.org>
- First RPM build
