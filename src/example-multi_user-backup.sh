#!/bin/sh

test ! -e /var/backups && mkdir /var/backups 

rdiff-backup /media/EXAMPLE /var/backups/EXAMPLE.rdiff || echo "back up incomplete `date`" >> /home/YOUR-USERNAME/EXAMPLE-backup.log

chown YOUR-USERNAME:users /var/backups/EXAMPLE.rdiff
