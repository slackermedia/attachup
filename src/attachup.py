#!/usr/bin/env python3

import re 
import os
import argparse
import pyudev
from sys import argv
from os.path import expanduser
import configparser

VERSION = '0.5.2'

def getOpt(argv):
    '''
    analizu datamoj
    '''
    
    ARGS = argv[1:]

    PARSE = argparse.ArgumentParser(description="Create new attachup rule and script.",argument_default=argparse.SUPPRESS)
    PARSE.add_argument("-i", "--uuid",nargs="?",dest='UUID',help="UUID of drive to backup.")
    PARSE.add_argument("-l", "--label",nargs="?",dest='LABEL',help="File system label of drive to backup.")
    PARSE.add_argument("-p", "--path",nargs="?",dest='PATH',help="Path to backup location. Default: /var/attachups")
    PARSE.add_argument("-v", "--verbose",dest='VERBOSE',action='store_true',help="Print debug messages.")
    PARSE.add_argument("-w", "--which",dest='WHICH',action='store_true',help="Print version.")
    PARSE.add_argument("-a", "--add",dest='ADD',action='store_true',help="Add a backup rule.")
    PARSE.add_argument("-d", "--delete",dest='DELETE',action='store_true',help="Delete a backup rule.")
    PARSE.add_argument("-s", "--list",dest='LIST',action='store_true',help="List current attachup rules.")
    PARSE.add_argument("-t", "--permanent",dest='PERM',action='store_true',help="Save options for this user to config file.")
    PARSE.add_argument("-r", "--rules",nargs="?",dest='RULE',help="Full path to rule file to modify. Default: /etc/udev/rules.d/81-attachup.rules")
    PARSE.add_argument("-c", "--conf",nargs="?",dest='CONF',help="Full path to alternate config file. Default: /etc/attachup.conf")
    PARSE.add_argument("-e", "--exec",nargs="?",dest='EPATH',help="Backup script destination directory. Default: /usr/local/bin")
    PARSE.add_argument("-u", "--user",nargs="?",dest='UNAM',help="Name of user owning this backup. Default: root")
    PARSE.add_argument("-g", "--group",nargs="?",dest='GRP',help="Name of group owning this backup. Default: root")
    PARSE.add_argument("-x", "--dev",dest='DEV',action='store_true',help="Dev mode. Do not use.")
    
    OPTIONS = PARSE.parse_args(ARGS)
    OPTS = vars(OPTIONS)
    return OPTS

def addRule(UUID,PATH,RULE,EPATH,LABEL,UNAM,GRP,DEV):
    '''
    add rule
    '''
    F=open(RULE,"a")
    F.write('\nACTION=="add",ENV{ID_FS_UUID}=="' + UUID + '",RUN+="/bin/mkdir -p /media/%E{dir_name}",RUN+="/sbin/mount -o $env{mount_options} /dev/%k /media/%E{dir_name}",MODE="0770",GROUP="plugdev",RUN+="/usr/local/bin/%E{dir_name}-attachup.sh"\n')
    F.close()

    F=open(os.path.join(EPATH,LABEL + '-attachup.sh'),"w+")
    F.write('#!/bin/sh\n')
    F.write('test ! -e ' + PATH + '/' + LABEL + '.rdiff && mkdir -p ' + PATH + '/' + LABEL + '.rdiff\n')
    if UNAM is "root":
        UNAMLOG="/tmp"
    else:
        UNAMLOG=expanduser("~")

    F.write('rdiff-backup /media/' + LABEL + ' ' + PATH + '/' + LABEL + '.rdiff || echo "back up incomplete `date`" >> ' + UNAMLOG + '/attachup.log\n')

    F.write('chown ' + UNAM + ':' + GRP + ' ' + PATH + '/' + LABEL + '.rdiff\n')
    F.close()

    # standard permissions
    if not DEV:
        os.chown(os.path.join(EPATH,LABEL + '-attachup.sh'), 0,0)
        os.chmod(os.path.join(EPATH,LABEL + '-attachup.sh'), 0o755)

        os.chown(os.path.join(RULE), 0,0)
        os.chmod(os.path.join(RULE), 0o644)

    print("Complete! You can try to reload your udev rules with `udevadm control --reload`")
    print("But more likely, you will have to reboot to achieve a full udev restart.")

def delRule(UUID,PATH,RULE,EPATH,LABEL):
    '''
    delete rule
    '''
    F = open(RULE,"r+")
    LINES = F.readlines()
    F.seek(0)
    for i in LINES:
        if UUID not in i:
            F.write(i)
    F.truncate()
    F.close()

    os.chmod(os.path.join(EPATH,LABEL + '-attachup.sh'), 0o600)

    print("Complete! Reload your udev rules with `udevadm control --reload`")

def udelve():
    print('You must provide a UUID and file system label for the partition you want to configure for backup.')
    print('Here is a list of what seems to be attached to your computer now:')
    print(' ')

    CONTEXT = pyudev.Context()
    for device in CONTEXT.list_devices(subsystem='block',DEVTYPE='partition'):
        print('{0} ({1})'.format(device.device_node,device.device_type),device.get('ID_FS_UUID'),device.get('ID_FS_LABEL', '[no label]'))
        exit()

def listRules(RULE):
    F = open(RULE,"r")
    L = F.readlines()
    for i in L:
        if 'UUID' in i:
            print(i.split('"')[3])
    F.close()
    exit()
    
def mkPerm(UUID,PATH,RULE,EPATH,LABEL,UNAM,GRP,CONF):
    CONFIG[UNAM] = {'username': UNAM,
                         'group': GRP,
                         'bin': EPATH,
                         'udev_dest': RULE,
                         'attachup_dir': PATH}

    with open(os.path.join(CONF),"w+") as F:
        CONFIG.write(F)


def do(argv):
    OPTS = getOpt(argv)
    OPTS['CONF'] = OPTS['CONF'] if 'CONF' in OPTS else os.path.join('etc','attachup.conf')

    OPTS['VERBOSE'] = OPTS['VERBOSE'] if 'VERBOSE' in OPTS else False

    if OPTS['VERBOSE']:
        print("Using conf file " + OPTS['CONF'])
        
    CONFIG = configparser.ConfigParser()
    CONFIG.read(OPTS['CONF'])

    OPTS['UNAM'] = OPTS['UNAM'] if 'UNAM' in OPTS else 'root'
    UNAM = OPTS['UNAM']
    OPTS['GRP'] = OPTS['GRP'] if 'GRP' in OPTS else 'root'

    try:
        CONFIG[UNAM] = CONFIG[UNAM] if UNAM in CONFIG else 'root'
    except:
        pass

    OPTS['VERBOSE'] = OPTS['VERBOSE'] if 'VERBOSE' in OPTS or 'VERBOSE' in CONFIG[UNAM] else False
    OPTS['RULE'] = OPTS['RULE'] if 'RULE' in OPTS or 'RULE' in CONFIG[UNAM] else os.path.join('/','etc','udev','rules.d','81-attachup.rules')
    OPTS['PATH'] = OPTS['PATH'] if 'PATH' in OPTS or 'PATH' in CONFIG[UNAM] else os.path.join('/','var','attachups')
    OPTS['EPATH'] = OPTS['EPATH'] if 'EPATH' in OPTS or 'EPATH' in CONFIG[UNAM] else os.path.join('/','usr','local','bin')
    
    OPTS['WHICH'] = OPTS['WHICH'] if 'WHICH' in OPTS else False
    OPTS['ADD'] = OPTS['ADD'] if 'ADD' in OPTS else False
    OPTS['DELETE'] = OPTS['DELETE'] if 'DELETE' in OPTS else False
    OPTS['UUID'] = OPTS['UUID'] if 'UUID' in OPTS else False
    OPTS['LABEL'] = OPTS['LABEL'] if 'LABEL' in OPTS else False
    OPTS['PERM'] = OPTS['PERM'] if 'PERM' in OPTS else False
    OPTS['DEV'] = OPTS['DEV'] if 'DEV' in OPTS else False
    
    if OPTS['WHICH']:
        print(VERSION)
    
    if OPTS['VERBOSE']:
        print('Rule file set to: ' + OPTS['RULE'])

    if OPTS['VERBOSE']:
        print('Using default path: /var/attachups')

    OPTS['LIST'] = OPTS['LIST'] if 'LIST' in OPTS else False
    if OPTS['LIST']:
        listRules(OPTS['RULE'])
        
    # required info check
    if OPTS['UUID'] and OPTS['LABEL']:
        pass
    else:
        udelve()

    # conflict check
    if OPTS['ADD'] and OPTS['DELETE']:
        print('Cannot add and delete the same rule.')
        exit()
    elif OPTS['ADD'] and not OPTS['DELETE']:
        addRule(OPTS['UUID'],OPTS['PATH'],OPTS['RULE'],OPTS['EPATH'],OPTS['LABEL'],OPTS['UNAM'],OPTS['GRP'],OPTS['DEV'])
    elif OPTS['DELETE'] and not OPTS['ADD']:
        delRule(OPTS['UUID'],OPTS['PATH'],OPTS['RULE'],OPTS['EPATH'],OPTS['LABEL'])
    else:
        pass

    if OPTS['PERM']:
        mkPerm(OPTS['UUID'],OPTS['PATH'],OPTS['RULE'],OPTS['EPATH'],OPTS['LABEL'],OPTS['UNAM'],OPTS['GRP'],OPTS['CONF'])

if __name__ == "__main__":
    do(argv)
