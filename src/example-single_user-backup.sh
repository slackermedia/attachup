#!/bin/sh

test ! -e /home/YOUR-USERNAME/EXAMPLE.rdiff && mkdir /home/YOUR-USERNAME/EXAMPLE.rdiff

rdiff-backup /media/EXAMPLE /home/YOUR-USERNAME/EXAMPLE.rdiff || echo "back up incomplete `date`" >> /home/YOUR-USERNAME/EXAMPLE-backup.log

chown YOUR-USERNAME:users /home/YOUR-USERNAME/EXAMPLE.rdiff
## optionally, reassert permissions if necessary:
# chmod -R 755 /home/YOUR-USERNAME/EXAMPLE.rdiff
