# Attachup

Don't backup. Attachup!

Yes, you heard right, backing up is out and Attachup is *in*!

## What is Attachup?

Attachup turns the concept of backing-up on its head. With Attachup, your production data lives on a portable drive (like a thumbdrive or pendrive), and your *computer* is your backup drive.

When you plug in your external drive into a computer with Attachup installed and configured, Attachup automatically backs up any new data and changes from your portable drive onto the drive of the computer.

Have more than one computer? Install Attachup on all your computers, and you'll have instant redundancy, with the master copy on your portable drive!

Has your computer died? Does't matter! the most current version of your data is on your portable drive! Revive your computer, reinstall Attachup, and *go, man, go*!

Has your thumbdrive died? Doesn't matter! the latest version of your data is on your computer!

## Install

The default build system is SlackBuild. If you're on Slackware, that means you can do this:

    $ su -
    # ./attachup.SlackBuild
    [ ... ]
    # installpkg /tmp/attachup-*-t?z

If you are not on Slackware, then you lack the ``makepkg`` command that Slackware provides, so generating a SlackBuild is not an option for you. That's OK, you can still do a manual install:

* move ``81-attachup.conf`` in ``/etc/udev/rules.d``
* move ``attachup.py`` to ``/usr/local/bin/attachup`` or thereabouts
* gzip and move the man pages to the appropriate man dirs

## Adding new drives

Attachup uses ``udev`` and ``rdiff-backup`` scripts to perform backups.

Attachup is also a command that manages your backup policy.

To add a policy for a drive:

    $ attachup --add --uuid 1234-5678-9555-5555 --label mydrive

To remove a policy for a drive:

    $ attachup --delete --uuid 1234-5678-9555-5555 --label mydrive

There are several options for user ownership, so see ``man attachup`` and ``man attachup.conf`` for details.


## How easy is it to restore from my backups?

Attachup actually only manages the drive itself, not the backups. It uses ``rdiff-backup`` to manage backups, so restoring files is done through [rdiff-backup](https://www.nongnu.org/rdiff-backup), which has great documentation. Here is a quick overview.

Restore the most-recently backed-up version of a file:

    $ rdiff-backup --restore-as-of now /var/backups/bob/foo.kra ~/foo.kra

The ``--restore-as-of`` option (``-r``) goes back is time farther than just the most recent backup. Maybe you think the best good version of a file existed two days ago:

    $ rdiff-backup --restore-as-of 2D /var/backups/bob/foo.kra ~/foo.kra

Or maybe you aren't sure, and would rather view a few different versions. It's all backed up, so you can resurrect as many versions as you like:

    $ rdiff-backup --restore-as-of 2D /var/backups/bob/foo.kra ~/foo-2D.kra
    $ rdiff-backup --restore-as-of 5D /var/backups/bob/foo.kra ~/foo-5D.kra
    
Other acceptable time formats include ``33m33s`` (33 minutes and 33 seconds ago) and full dates like ``2017-01-01`` for that New Years photo that you wanted to forget but are now having second thoughts about.

If you know even more detail about your file, use ``rdiff-backup`` to restore directly from an incremental backup file. Increment files are stored in your backup destination in rdiff-backup-data/increments. They hold the previous versions of changed files, so you can specify one directly:

    $ rdiff-backup /var/backups/bob/rdiff-backup-data/increments/novel.2014-11-24T02:21:41-07:00.diff.gz ~


## Who uses this?

I bought a 64 GB thumbdrive five years ago, and it houses my important data. I back the drive up to each of my computers. It's been reliable enough to save a few important files, and it's made any kind of computer failure basically meaningless to me. If my computer dies, I restore it from my system backups, and then move on. *Zero data loss* in five years.

For three years, Attachup was also used by myself and a few production managers at a large movie studio, in New Zealand, that I won't name for fear of insinuating endorsement.


## Requirements

Currently, Attachup runs only on Linux because I don't know how to handle external devices on other operating systems.

* Linux
* [librsync](http://librsync.sourceforge.net/)
* [rdiff-backup](http://rdiff-backup.nongnu.org/)
* ``udev`` (already installed on Linux)
* ``pyudev`` (sometimes called ``python3-udev`` or similar)

## Bugs

File bugs in Gitlab, or email me directly.

